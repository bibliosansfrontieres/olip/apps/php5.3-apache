FROM buildpack-deps:jessie
# hadolint global ignore=DL3008

RUN sed -i -e '/^deb/d;s/^# //;s/^deb /deb \[trusted=yes\] /' /etc/apt/sources.list

# hadolint ignore=DL3015
RUN apt-get update \
	&& apt-get install --yes --no-install-recommends \
		apache2-bin \
		apache2-dev \
		apache2.2-common \
	&& rm -rf /var/lib/apt/lists/* \
	\
	&& a2dismod mpm_event \
	&& a2enmod mpm_prefork \
	\
	&& rm -rf /var/www/html \
	&& mkdir -p \
		/var/lock/apache2 \
		/var/run/apache2 \
		/var/log/apache2 \
		/var/www/html \
	&& chown -R www-data:www-data \
		/var/lock/apache2 \
		/var/run/apache2 \
		/var/log/apache2 \
		/var/www/html
COPY apache2.conf /etc/apache2/apache2.conf


ENV PHP_VERSION 5.3.29
ENV PHP_INI_DIR /usr/local/lib

# php 5.3 needs older autoconf
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=DL3003
RUN set -x \
	&& mkdir -p $PHP_INI_DIR/conf.d \
	\
	&& apt-get update \
	&& apt-get install --yes --no-install-recommends \
		autoconf2.13 \
	&& rm -r /var/lib/apt/lists/* \
	\
	&& ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
	&& case "$ARCH" in \
		amd64) 	libbison='http://archive.ubuntu.com/ubuntu/pool/main/b/bison/libbison-dev_3.0.2.dfsg-2_amd64.deb' ;	bison='http://archive.ubuntu.com/ubuntu/pool/main/b/bison/bison_3.0.2.dfsg-2_amd64.deb' ; 	libdir='lib/x86_64-linux-gnu' ;; \
		i386)	libbison='http://archive.ubuntu.com/ubuntu/pool/main/b/bison/libbison-dev_3.0.2.dfsg-2_i386.deb' ; 	bison='http://archive.ubuntu.com/ubuntu/pool/main/b/bison/bison_3.0.2.dfsg-2_i386.deb' ; 	libdir='lib/i386-linux-gnu' ;; \
		armhf) 	libbison='http://ports.ubuntu.com/pool/main/b/bison/libbison-dev_3.0.2.dfsg-2_armhf.deb' ; 			bison='http://ports.ubuntu.com/pool/main/b/bison/bison_3.0.2.dfsg-2_armhf.deb' ; 			libdir='lib/arm-linux-gnueabihf' ;; \
		arm64) 	libbison='http://ports.ubuntu.com/pool/main/b/bison/libbison-dev_3.0.2.dfsg-2_arm64.deb' ; 			bison='http://ports.ubuntu.com/pool/main/b/bison/bison_3.0.2.dfsg-2_arm64.deb' ; 			libdir='lib/arm-linux-gnueabihf' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac \
	&& curl --insecure -SLO "$libbison" \
	&& curl --insecure -SLO "$bison" \
	&& dpkg -i ./libbison-*.deb \
	&& dpkg -i ./bison*.deb \
	&& rm ./*.deb \
	\
	&& curl --insecure -SL "http://php.net/get/php-$PHP_VERSION.tar.bz2/from/this/mirror" -o php.tar.bz2 \
	&& mkdir -p /usr/src/php \
	&& tar -xf php.tar.bz2 -C /usr/src/php --strip-components=1 \
	&& rm ./php.tar.bz2* \
	\
	&& apt-get install --yes --no-install-recommends \
		libmysqlclient-dev \
	&& cd /usr/src/php \
	&& ./buildconf --force \
	&& ./configure --disable-cgi \
		"$(command -v apxs2 > /dev/null 2>&1 && echo '--with-apxs2' || true)" \
    --with-config-file-path="$PHP_INI_DIR" \
    --with-config-file-scan-dir="$PHP_INI_DIR/conf.d" \
		--with-mysql \
		--with-mysqli \
		--with-pdo-mysql \
		--with-libdir="$libdir" \
	&& make -j"$(nproc)" \
	&& make install \
	&& dpkg -r bison libbison-dev \
	\
	&& apt-get purge --yes --auto-remove \
		autoconf2.13 \
	&& make clean

COPY docker-php-* /usr/local/bin/
COPY apache2-foreground /usr/local/bin/

WORKDIR /var/www/html

EXPOSE 80
CMD ["apache2-foreground"]

