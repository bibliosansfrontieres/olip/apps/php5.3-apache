# offlineinternet/php:5.3-apache

PHP 5.3 [reached EOL](http://php.net/eol.php) mid 2014 and
official Docker support was [dropped](https://github.com/docker-library/php/pull/20).
We still needed to run 5.3 for some OLIP App,
so here comes this image based on the latest official builds of PHP.

## This image

- based off Debian 8 Jessie
- multiarch: `amd64`, `armhf`, `i386` (untested), `arm64` (untested)
- is **not** an OLIP-base base,
  i.e. it lacks all of the content management script et al.
  It's a **bare** PHP5.3/Apache image.

## Credits

This is heavily based on [eugeneware/docker-php-5.3-apache](https://github.com/eugeneware/docker-php-5.3-apache)
